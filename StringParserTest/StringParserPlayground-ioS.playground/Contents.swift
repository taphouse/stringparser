//: Playground - noun: a place where people can play

import UIKit
import StringParseriOS

let renderer = StringRenderer.standard()
renderer.render(text: "some *bold* and ~struck through~ text from me to <@Jared> but `1234` needs to be code printed")

renderer.render(text: "~*both*~")
