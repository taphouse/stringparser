//
//  ViewController.swift
//  StringParserTest
//
//  Created by Jared Sorge on 3/23/17.
//  Copyright © 2017 Taphouse Software. All rights reserved.
//

import UIKit
import StringParseriOS

class ViewController: UIViewController {

    private var renderer = Renderer()
    
    @IBOutlet private var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
/*        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        label.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        label.numberOfLines = 0
  */
        let renderer = StringRenderer.standard()

        let attributedText = renderer.render(text: "some *bold* and ~struck through~ `text from me` to <@Jared> but `code 1234` _needs_ to be _code printed_")
        label.attributedText = attributedText
    }
}

