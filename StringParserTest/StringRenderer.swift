//
//  StringParser.swift
//  Tightrope
//
//  Created by Jared Sorge on 3/21/17.
//  Much credit goes to Tim Ekl
//  Copyright © 2017 Taphouse Software. All rights reserved.
//

import Foundation

public typealias Attributes = [String: Any]
public typealias AttributeKeys = [String]

fileprivate struct Checkpoint {
    let startingLocation: Int
    let endingLocation: Int?
    let token: RecognizerToken
    let attributes: Attributes?
}

public class Renderer {
    //MARK: API
    public init() {}
    
    public func attach(_ recognizer: Recognizer) {
        recognizers.append(recognizer)
        
        recognizers.sort { (first, _) -> Bool in
            return first is ExclusiveSingleAttributeRecognizer
        }
    }
    
    public func render(text: String) -> NSAttributedString {
        self.input = NSMutableString(string: text)
        attributeCheckpoints = []
        
        var continuation: Continuation = .all(from: 0)
        while true {
            switch continuation {
            case .none:
                return produceOutput()
            case .possible(let recognizer, let position):
                continuation = enumerateInputForPossible(recognizer, from: position)
            case .exclusive(let recognizer, let position):
                guard let exclusive = recognizer as? ExclusiveSingleAttributeRecognizer else { break }
                continuation = enumerateInputForExclusive(exclusive, from: position)
            case .all(let position):
                continuation = enumerateInputForAll(from: position)
            }
        }
        
        
    }
    
    //MARK: Private
    fileprivate var input: NSMutableString = NSMutableString(string: "")
    
    private var attributeCheckpoints: [Checkpoint] = []
    
    private var recognizers: [Recognizer] = []
    
    /// MARK: Rendering state
    /// Call this once all the attribute checkpoints are set up and input enumeration is done
    private func produceOutput() -> NSAttributedString {
        guard attributeCheckpoints.count > 0 else { return NSAttributedString(string: input as String) }
        let result = NSMutableAttributedString(string: input as String)
        
        for checkpoint in attributeCheckpoints {
            guard var endLocation = checkpoint.endingLocation,
                let attributes = checkpoint.attributes
                else { continue }
            let startLocation = checkpoint.startingLocation
            if startLocation > endLocation {
                endLocation = startLocation
            }
            
            var length = endLocation - startLocation
            if length - startLocation > input.length {
                length = input.length
            }
            let range = NSRange(location: startLocation, length: length)
            result.addAttributes(attributes, range: range)
        }
        
        return result
    }
    
    private enum Continuation {
        case none // done
        case possible(for: Recognizer, from: Int) // one Recognizer has claimed it *might* be starting or ending
        case exclusive(for: Recognizer, from: Int) // one Recognizer has *definitely* started an exclusive range
        case all(from: Int) // nobody is possibly changing states or has claimed exclusivity, so feed text to everyone
    }
    
    private func enumerateInputForAll(from position: Int) -> Continuation {
        var result: Continuation = .none
        
        guard position <= input.length - 1 else {
            return result
        }
        
        input.enumerateSubstrings(in: NSRange(location: position, length: input.length - position), options: .byComposedCharacterSequences) { (substring, range, _, stop) in
            guard let substring = substring else {
                return
            }
            
            for recognizer in self.recognizers {
                switch recognizer.accept(substring as String) {
                case .unrecognized:
                    break
                    
                case .begin(let token):
                    self.handleBegin(startingWith: substring, from: range, using: token, at: range.location, replacingTokens: recognizer.removeTokens)
                    
                case .end(let output):
                    self.handleEnd(startingWith: substring, from: range, using: output.token, applying: output.transformer, replacingTokens: recognizer.removeTokens)
                    
                case .possible:
                    result = .possible(for: recognizer, from: range.location)
                    stop.initialize(to: true)
                    return
                    
                case .beginExclusive(_):
                    result = .exclusive(for: recognizer, from: range.location)
                    stop.initialize(to: true)
                    return
                    
                case .endExclusive(_):
                    break
                }
            }
        }
        
        return result
    }
    
    private func enumerateInputForPossible(_ recognizer: Recognizer, from position: Int) -> Continuation {
        var result: Continuation = .none
        
        input.enumerateSubstrings(in: NSRange(location: position, length: input.length - position), options: .byComposedCharacterSequences) { (substring, range, _, stop) in
            let from = position
            let to = range.location + range.length
            let difference = to - from
            let lookaheadRange = NSRange(location: from, length: difference)
            let lookaheadSequence = self.input.substring(with: lookaheadRange)
            
            switch recognizer.accept(lookaheadSequence as String) {
            case .unrecognized:
                result = .all(from: position + 1)
                stop.initialize(to: true)
                return
                
            case .possible:
                break
                
            case .begin(let token):
                self.handleBegin(startingWith: lookaheadSequence, from: lookaheadRange, using: token, at: from, replacingTokens: recognizer.removeTokens)
                
            case .end(let output):
                self.handleEnd(startingWith: substring, from: range, using: output.token, applying: output.transformer, replacingTokens: recognizer.removeTokens)
                
            case .beginExclusive(_):
                fallthrough
                
            case .endExclusive(_):
                fatalError("Unimplemented")
            }
        }
        
        return result
    }
    
     private func enumerateInputForExclusive(_ recognizer: ExclusiveSingleAttributeRecognizer, from position: Int) -> Continuation {
        var result: Continuation = .none
        
        input.enumerateSubstrings(in: NSRange(location: position, length: input.length - position), options: .byComposedCharacterSequences) { (substring, range, _, stop) in
            guard let substring = substring else { return }
            
            switch recognizer.acceptAsExclusive(substring as String) {
                
            case .begin(let token):
                self.handleBegin(startingWith: substring, from: range, using: token, at: position, replacingTokens: recognizer.removeTokens)
                
            case .endExclusive(let output):
                result = self.handleEnd(startingWith: substring, from: range, using: output.token, applying: output.transformer, replacingTokens: recognizer.removeTokens)
                stop.initialize(to: true)
                return
                
            default:
                break
            }
        }
        
        return result
     }
    
    private func handleBegin(startingWith substring: String?, from range: NSRange, using token: RecognizerToken, at position: Int, replacingTokens: Bool) {
        if replacingTokens {
            if input.substring(with: range) == substring {
                input.replaceCharacters(in: range, with: "")
            }
        }
        
        let checkpoint = Checkpoint(startingLocation: position, endingLocation: nil, token: token, attributes: nil)
        attributeCheckpoints.append(checkpoint)
    }
    
    @discardableResult private func handleEnd(startingWith substring: String?, from range: NSRange, using token: RecognizerToken, applying transformation: RecognizerTransformation, replacingTokens: Bool) -> Continuation {
        let checkpointIndex = attributeCheckpoints.index { $0.token == token }
        guard let index = checkpointIndex else { return .all(from: range.location + range.length + 1) }
        let checkpoint = attributeCheckpoints[index]
        
        
        var endPosition = range.location + range.length
        if endPosition > input.length - 1 {
            endPosition = input.length - 1
        }

        if replacingTokens {
            if input.substring(with: range) == substring {
                input.replaceCharacters(in: range, with: "")
                endPosition -= range.length
                
                //If the ending position is at the end of the string, the resulting range of attributes needs to cover the last character
                if endPosition == input.length - 1 {
                    endPosition += 1
                }
            }
        }
        
        let subRange = NSRange(location: checkpoint.startingLocation, length: endPosition - checkpoint.startingLocation)
        let substring = input.substring(with: subRange)
        let output = transformation(substring)
        
        if let updatedOutput = output.outputString {
            input.replaceCharacters(in: subRange, with: updatedOutput)
            
            let delta = (updatedOutput as NSString).length - (substring as NSString).length
            endPosition += delta
        }
        
        let updatedCheckpoint = Checkpoint(startingLocation: checkpoint.startingLocation, endingLocation: endPosition, token: token, attributes: output.attributes)
        attributeCheckpoints[index] = updatedCheckpoint

        return .all(from: endPosition + 1)
    }
}

