//
//  AppDelegate.swift
//  StringParserTest
//
//  Created by Jared Sorge on 3/23/17.
//  Copyright © 2017 Taphouse Software. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
}

