//
//  StringParserPlayground-Mac.h
//  StringParserPlayground-Mac
//
//  Created by Jared Sorge on 3/23/17.
//  Copyright © 2017 Taphouse Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for StringParserPlayground-Mac.
FOUNDATION_EXPORT double StringParserPlayground_MacVersionNumber;

//! Project version string for StringParserPlayground-Mac.
FOUNDATION_EXPORT const unsigned char StringParserPlayground_MacVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <StringParserPlayground_Mac/PublicHeader.h>


