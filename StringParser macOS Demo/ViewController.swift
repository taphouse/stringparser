//
//  ViewController.swift
//  StringParser macOS Demo
//
//  Created by Jared Sorge on 3/24/17.
//  Copyright © 2017 Taphouse Software. All rights reserved.
//

import Cocoa
import StringParserMac

class ViewController: NSViewController {

    @IBOutlet weak var label: NSTextField!
    
    private let renderer = StringRenderer.standard()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func textFieldChanged(_ textField: NSTextField) {
        label.attributedStringValue = renderer.render(text: textField.stringValue)
    }
}

