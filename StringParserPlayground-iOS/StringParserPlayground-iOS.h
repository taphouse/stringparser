//
//  StringParserPlayground-iOS.h
//  StringParserPlayground-iOS
//
//  Created by Jared Sorge on 3/23/17.
//  Copyright © 2017 Taphouse Software. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for StringParserPlayground-iOS.
FOUNDATION_EXPORT double StringParserPlayground_iOSVersionNumber;

//! Project version string for StringParserPlayground-iOS.
FOUNDATION_EXPORT const unsigned char StringParserPlayground_iOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <StringParserPlayground_iOS/PublicHeader.h>


