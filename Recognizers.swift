//
//  Recognizers.swift
//  StringParser
//
//  Created by Jared Sorge on 3/26/17.
//  Copyright © 2017 Taphouse Software. All rights reserved.
//

import Foundation

public typealias RecognizerToken = UUID
public typealias RecognizerTransformation = (_: String) -> (attributes: Attributes, outputString: String?)
public typealias RecognizerOutput = (token: UUID, transformer: RecognizerTransformation)

public enum RecognitionResult {
    case unrecognized
    case possible
    case begin(token: RecognizerToken)
    case end(output: RecognizerOutput)
    case beginExclusive(token: RecognizerToken)
    case endExclusive(output: RecognizerOutput)
}

public protocol Recognizer {
    var removeTokens: Bool { get }
    func accept(_ composedCharacterSequence: String) -> RecognitionResult
    var finalTransformation: RecognizerTransformation { get }
}

/// Use this class for non-exclusive attributes
public class BoundedSingleAttributeRecognizer: Recognizer {
    public var prefix: String
    public var suffix: String
    
    public init(prefix: String, suffix: String, transformer: @escaping RecognizerTransformation) {
        self.prefix = prefix
        self.suffix = suffix
        self.finalTransformation = transformer
    }
    
    //MARK: Recognizer
    public var removeTokens: Bool = true
    
    public let finalTransformation: RecognizerTransformation
    
    public func accept(_ sequence: String) -> RecognitionResult {
        if sequence == prefix, _state != .matchedBeginning {
            _state = .matchedBeginning
            _currentToken = UUID()
            return .begin(token: _currentToken!)
        } else if sequence == suffix, _state == .matchedBeginning {
            _state = .none
            let token = _currentToken!
            _currentToken = nil
            return .end(output: (token: token, transformer: finalTransformation))
        } else if prefix.hasPrefix(sequence) || suffix.hasPrefix(sequence) {
            return .possible
        } else {
            return .unrecognized
        }
    }
    
    //MARK: Private
    fileprivate enum CurrentState {
        case none
        case matchedBeginning
    }
    fileprivate var _state: CurrentState = .none
    fileprivate var _currentToken: UUID?
}

/// Use this class for applying attributes to a range, and ignoring any attributes that may lie within that range
/// For example, `*bold*` would only have attributes applied to the "*bold*" characters.
public class ExclusiveSingleAttributeRecognizer: BoundedSingleAttributeRecognizer {
    public override func accept(_ sequence: String) -> RecognitionResult {
        if sequence == prefix, _state != .matchedBeginning {
            _state = .matchedBeginning
            _currentToken = UUID()
            return .beginExclusive(token: _currentToken!)
        } else if sequence == suffix {
            _state = .none
            let token = _currentToken!
            _currentToken = nil
            return .end(output: (token: token, transformer: finalTransformation))
        } else if prefix.hasPrefix(sequence) || suffix.hasPrefix(sequence) {
            return .possible
        } else {
            return .unrecognized
        }
    }
    
    private var _exclusiveState: CurrentState = .none
    public func acceptAsExclusive(_ sequence: String) -> RecognitionResult {
        if sequence == prefix, _exclusiveState != .matchedBeginning {
            _exclusiveState = .matchedBeginning
            _currentToken = _currentToken ?? UUID()
            return .begin(token: _currentToken!)
        } else if sequence == suffix {
            _exclusiveState = .none
            _state = .none
            let token = _currentToken!
            _currentToken = nil
            return .endExclusive(output: (token: token, transformer: finalTransformation))
        } else if prefix.hasPrefix(sequence) || suffix.hasPrefix(sequence) {
            return .possible
        } else {
            return .unrecognized
        }
    }
}
