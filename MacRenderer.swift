//
//  MacRenderer.swift
//  StringParserTest
//
//  Created by Jared Sorge on 3/24/17.
//  Copyright © 2017 Taphouse Software. All rights reserved.
//

import Cocoa

public struct StringRenderer {
    public static let bold = BoundedSingleAttributeRecognizer(prefix: "*", suffix: "*") { (input) -> (attributes: Attributes, outputString: String?) in
        return (attributes: [NSFontAttributeName: NSFont.boldSystemFont(ofSize: 12)], outputString: nil)
    }
    
    public static let strikethrough = BoundedSingleAttributeRecognizer(prefix: "~", suffix: "~")  { (input) -> (attributes: Attributes, outputString: String?) in
        return (attributes: [NSStrikethroughStyleAttributeName: NSNumber(integerLiteral: 1)], outputString: nil)
    }
    
    public static let userToken = BoundedSingleAttributeRecognizer(prefix: "<@", suffix: ">")  { (input) -> (attributes: Attributes, outputString: String?) in
        return (attributes: [NSForegroundColorAttributeName: NSColor.blue], outputString: nil)
    }
    
    public static let codePrint = ExclusiveSingleAttributeRecognizer(prefix: "`", suffix: "`")  { (input) -> (attributes: Attributes, outputString: String?) in
        return (attributes: [NSFontAttributeName: NSFont(name: "Courier", size: 12)!], outputString: nil)
    }
    
    public static let italic = BoundedSingleAttributeRecognizer(prefix: "_", suffix: "_")  { (input) -> (attributes: Attributes, outputString: String?) in
        return (attributes: [NSFontAttributeName: NSFontManager.shared().font(withFamily: "Helvetica", traits: [.italicFontMask], weight: Int(NSFontWeightMedium), size: 12)!], outputString: nil)
    }
    
    public static func standard() -> Renderer {
        let renderer = Renderer()
        renderer.attach(bold)
        bold.removeTokens = false
        renderer.attach(strikethrough)
        renderer.attach(userToken)
        renderer.attach(codePrint)
        renderer.attach(italic)
        
        return renderer
    }
}
