//
//  iOSRenderer.swift
//  StringParserTest
//
//  Created by Jared Sorge on 3/24/17.
//  Copyright © 2017 Taphouse Software. All rights reserved.
//

import UIKit

public struct StringRenderer {
    public static let bold = BoundedSingleAttributeRecognizer(prefix: "*", suffix: "*") { (input) -> (attributes: Attributes, outputString: String?) in
        return (attributes: [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 12)], outputString: nil)
    }
    
    public static let strikethrough = BoundedSingleAttributeRecognizer(prefix: "~", suffix: "~")  { (input) -> (attributes: Attributes, outputString: String?) in
        return (attributes: [NSStrikethroughStyleAttributeName: NSNumber(integerLiteral: 1)], outputString: nil)
    }
    
    public static let userToken = BoundedSingleAttributeRecognizer(prefix: "<@", suffix: ">")  { (input) -> (attributes: Attributes, outputString: String?) in
        return (attributes: [NSForegroundColorAttributeName: UIColor.blue], outputString: nil)
    }
    
    public static let codePrint = ExclusiveSingleAttributeRecognizer(prefix: "`", suffix: "`")  { (input) -> (attributes: Attributes, outputString: String?) in
        return (attributes: [NSFontAttributeName: UIFont.monospacedDigitSystemFont(ofSize: 17, weight: 1.0)], outputString: nil)
    }
    
    public static let italic = BoundedSingleAttributeRecognizer(prefix: "_", suffix: "_")  { (input) -> (attributes: Attributes, outputString: String?) in
        return (attributes: [NSFontAttributeName: UIFont.italicSystemFont(ofSize: 12)], outputString: nil)
    }
    
    public static func standard() -> Renderer {
        let renderer = Renderer()
        renderer.attach(codePrint)
        renderer.attach(bold)
        renderer.attach(italic)
        renderer.attach(strikethrough)
        renderer.attach(userToken)
        
        return renderer
    }
}
