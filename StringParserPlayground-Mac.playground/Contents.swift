//: Playground - noun: a place where people can play

import Cocoa
import StringParserMac

let renderer = StringRenderer.standard()
renderer.render(text: "some *bold* and ~struck through~ text from me to <@Jared> but `*this*` needs to be `code printed`")

renderer.render(text: "~*both*~")
